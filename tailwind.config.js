const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    purge: [
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
    ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
        backgroundImage: {
            'categoryBg': "url('/img/categoryBg.png')",
            'defaultUserImage': "url('/img/defaultUserImage.svg')",
            'defaultImage': "url('/img/defaultImage.png')",
            'homeImage': "url('/img/homeimage.jpeg')",
        },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
      require('@tailwindcss/forms'),
      require('tailwindcss-rtl')
  ],
}
