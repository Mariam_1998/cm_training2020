<x-layouts.admin>
    <x-feedback/>
    <x-form action="{{ route('donations.update',['donation'=>$donation->id]) }}" method="PUT" has-files>
        <div class="bg-white shadow-lg rounded px-8 pt-6 pb-8 mb-4 flex flex-col m-5">
            <div class="-mx-3 md:flex">
                <div class="md:w-1/2 px-3">
                    <label>{{__('type').' '.__('Payment')}}</label>
                    <x-select class="ml-4 select" name="payment_type" id="payment_type"
                              label="{{__('type').' '.__('Payment')}}">
                        <option value="1" {{$donation->payment_type === '1' ? "selected" : "" }}>{{__('Transfer').' '.__('Bank')}}</option>
                        <option value="2" {{$donation->payment_type === '2' ? "selected" : "" }}>{{__('Deposit')}}</option>
                    </x-select>
                </div>
                <div class="md:w-1/2 px-3">
                    <label>{{__('date').' '.__('Deposit or Bank Transfer')}}</label>
                    <x-input class="text-right" name="date" value="{{$donation->date}}" type="date"/>
                </div>
            </div>
            <div class="-mx-3 md:flex">
                <div class="md:w-1/2 px-3">
                    <label>{{__('From Bank')}}</label>
                    <x-input name="from_account" value="{{$donation->from_account}}" placeholder="{{__('From Bank')}}" type="number"/>
                </div>
                <div class="md:w-1/2 px-3">
                    <label>{{__('To Bank')}}</label>
                    <x-select class="ml-4 select" name="account_id" id="account_id"
                              label="{{__('To Bank')}}">
                        @foreach($accounts as $account)
                            <option value="{{ $account->id }}" {{$account->id == $donation->account_id  ? 'selected' : ''}}>
                                {{ $account->name }}</option>
                        @endforeach
                    </x-select>
                </div>
            </div>

            <div class="-mx-3 md:flex ">
                <div class="md:w-1/2 px-3 mb-6 md:mb-0">
                    <label>{{__('Name')}}</label>
                    <x-input name="name" value="{{$donation->name}}" placeholder="{{__('Name')}}"/>
                </div>
                <div class="md:w-1/2 px-3 mb-6 md:mb-0">
                    <label>{{__('Phone')}}</label>
                    <x-input name="phone" value="{{$donation->phone}}" placeholder="{{__('Phone')}}" type="tel"/>
                </div>
            </div>

            <div class="-mx-3 md:flex ">
                <div class="md:w-1/2 px-3">
                    <label>{{__('Project')}}</label>
                    <x-select class="ml-4 select" name="category_id" id="category_id"
                              label="{{__('Project')}}">
                        @foreach($projects as $project)
                            <option value="{{ $project->id }}"
                                    {{$project->id == $donation->category_id  ? 'selected' : ''}}>{{ $project->name }}</option>

                        @endforeach
                    </x-select>
                </div>
                <div class="md:w-1/2 px-3">
                    <label>{{__('Amount')}}</label>
                    <x-input name="amount" value="{{$donation->amount}}" placeholder="{{__('Amount')}}" type="number"/>
                </div>
            </div>

            <div class="">
                @if($donation->hasMedia('donations') )
                    @foreach($donation->getMedia('donations') as $attachment)
                        {{$attachment}}
                    @endforeach
                @endif
            </div>

            <div class="mt-5">
                <button type="submit" class="btn">
                    {{__('Edit')}}
                </button>
                <input type="button" class="btn-cancel mt-3"
                       name="cancel" value="{{__('Cancel')}}" onClick="window.location.replace('/donations/manage');"/>
            </div>
        </div>
    </x-form>

</x-layouts.admin>



