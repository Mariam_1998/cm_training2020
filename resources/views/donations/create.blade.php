<x-layouts.base>
    @section('content')
        <x-feedback/>
        <div class="bg-white shadow-lg rounded px-8 pt-6 pb-8 mb-4 flex flex-col m-5">
            <h1>{{__('Create').' '.__('Donation')}}</h1>

            <x-form action="{{url('donations')}}" method="post" has-files>

                <div class="-mx-3 md:flex">
                    <div class="md:w-1/2 px-3">
                        <label>{{__('type').' '.__('Payment')}}</label>
                        <x-select name="payment_type" id="payment_type"
                                  label="{{__('type').' '.__('Payment')}}">
                            <option value="1">{{__('Transfer').' '.__('Bank')}}</option>
                            <option value="2">{{__('Deposit')}}</option>
                        </x-select>
                    </div>
                    <div class="md:w-1/2 px-3">
                        <label>{{__('date').' '.__('Deposit or Bank Transfer')}}</label>
                        <x-input name="date" class="text-right" value="{{ old('date') }}" type="date"/>
                    </div>
                </div>
                <div class="-mx-3 md:flex">
                    <div class="md:w-1/2 px-3">
                        <label>{{__('From Bank')}}</label>
                        <x-input name="from_account" value="{{ old('from_account') }}" type="number"/>
                    </div>
                    <div class="md:w-1/2 px-3">
                        <label>{{__('To Bank')}}</label>
                            <x-select name="account_id" id="account_id"
                                      label="{{__('To Bank')}}">
                                @foreach($accounts as $account)
                                    <option value="{{$account->id}}">{{$account->name}}</option>
                                @endforeach
                            </x-select>
                        </div>
                    </div>


                <div class="-mx-3 md:flex">
                    <div class="md:w-1/2 px-3 mb-6 md:mb-0">
                        <label>{{__('Name')}}</label>
                        <x-input name="name" value="{{ old('name') }}"/>
                    </div>
                    <div class="md:w-1/2 px-3 mb-6 md:mb-0">
                        <label>{{__('Phone')}}</label>
                        <x-input name="phone" value="{{ old('phone') }}" type="tel"/>
                    </div>
                </div>
                <div class="-mx-3 md:flex">
                    <div class="md:w-1/2 px-3">
                        <label>{{__('Project')}}</label>
                        <x-select name="category_id" id="category_id"
                                  label="{{__('Project')}}">
                            @foreach($projects as $project)
                                <option value="{{ $project->id }}">{{ $project->name }}</option>
                            @endforeach
                        </x-select>
                    </div>
                    <div class="md:w-1/2 px-3">
                        <label>{{__('Amount')}}</label>
                        <x-input name="amount" value="{{ old('amount') }}" type="number"/>
                    </div>
                </div>
                <div class="field">
                    <label class="text-lg font-extrabold leading-8 tracking-tight text-gray-800 sm:text-lg sm:leading-9"
                           for="image">{{__('Attach').' '.__('Image').' '.__('Deposit')}}</label>
                    <div class="mt-3">
                        <x-input type="file" id="image" name="image"/>
                    </div>
                </div>

                <div class="mt-5">
                    <button type="submit" class="btn">
                        {{__('Create')}}
                    </button>

                    <input type="button" class="btn-cancel"
                           name="cancel" value="{{__('Cancel')}}" onClick="window.location.replace ('/donations/manage');"/>
                </div>
            </x-form>
        </div>


    @endsection
</x-layouts.base>
