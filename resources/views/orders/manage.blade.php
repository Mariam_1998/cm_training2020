<x-manage model="orders" action="orders/create" :data="$orders" modelName="Order">
    <div class="w-full mx-5 print-area">
        <livewire:orders-datatable/>
    </div>
</x-manage>
