<x-layouts.admin>
    <x-feedback/>
    <h1>{{__('Edit')}}</h1>
    <x-form action="{{ route('orders.update',['order'=>$order->slug]) }}" method="PUT" has-files>
        <div class="bg-white shadow-lg rounded px-8 pt-6 pb-8 mb-4 flex flex-col my-2  print-area">
            <label>{{__('Title')}}</label>
            <x-input name="title" value="{{$order->title}}" placeholder="{{__('Title')}}"/>

            <label>{{__('Problem')}}</label>
            <x-input name="problem" value="{{$order->problem}}" placeholder="{{__('Problem')}}"/>

            <label>{{__('Device')}}</label>
            <x-input name="device" value="{{$order->device}}" placeholder="{{__('Device')}}"/>

            <label class="label">{{__('type').' '.__('Order')}}</label>
            <x-select class="ml-4 select" name="type" id="type"
                      label="{{__('type').' '.__('order')}}">
                <option value="1" {{$order->type === '1' ? "selected" : "" }}>{{__('New')}}</option>
                <option value="2" {{$order->type === '2' ? "selected" : "" }}>{{__('Maintenance')}}</option>
            </x-select>

            <label>{{__('Address')}}</label>
            <x-input name="address" value="{{$order->address}}" placeholder="{{__('Address')}}"/>

            <label class="label">{{__('state').' '.__('Order')}}</label>
            <x-select class="ml-4 select" name="state" id="state"
                      label="{{__('state').' '.__('order')}}">
                <option value="1" {{$order->state === '1' ? "selected" : "" }}>{{__('New')}}</option>
                <option value="2" {{$order->state === '2' ? "selected" : "" }}>{{__('In Progress')}}</option>
                <option value="3" {{$order->state === '3' ? "selected" : "" }}>{{__('Closed')}}</option>
                <option value="4" {{$order->state === '4' ? "selected" : "" }}>{{__('Rejected')}}</option>
            </x-select>

            <label>{{__('Notes')}}</label>
            <textarea row="4"
                      class="w-full h-40 px-5 py-3 border border-gray-400 rounded-lg outline-none focus:shadow-outline"
                      name="notes">{{$order->notes}}</textarea>

            <div class="mt-3">
                <button type="submit" class="btn">
                    {{__('Edit')}}
                </button>
                <button type="submit" class="btn" onclick="myFunction()">
                    {{__('Print')}}
                </button>
                <input type="button" class="btn-cancel"
                       name="cancel" value="{{__('Cancel')}}" onClick="window.location.replace('/orders/manage');"/>
            </div>
        </div>
    </x-form>
    <script>
        function myFunction() {
            window.print();
        }
    </script>
</x-layouts.admin>



