<x-layouts.app>
    <div class="m-5 w-full">

        <h1>{{$order->title}}</h1>
        <div class="flex">
            <p class="mt-3 text-base leading-6 text-gray-900 ml-2">
                {{__('Problem')}}:
            </p>
            <p class="mt-3 text-base leading-6 text-gray-500 pl-3 text-justify">
                {{$order->problem}}
            </p>
        </div>
        <div class="flex">
            <p class="mt-3 text-base leading-6 text-gray-900 ml-2">
                {{__('Device')}}:
            </p>
            <p class="mt-3 text-base leading-6 text-gray-500 pl-3 text-justify">
                {{$order->device}}
            </p>
        </div>
        <div class="flex">
            <p class="mt-3 text-base leading-6 text-gray-900 ml-2">
                {{__('Type')}}:
            </p>
            <p class="mt-3 text-base leading-6 text-gray-500 pl-3 text-justify">
                {{$order->type}}
            </p>
        </div>
        <div class="flex">
            <p class="mt-3 text-base leading-6 text-gray-900 ml-2">
                {{__('Address')}}:
            </p>
            <p class="mt-3 text-base leading-6 text-gray-500 pl-3 text-justify">
                {{$order->address}}
            </p>
        </div>

        <div class="flex">
            <p class="mt-3 text-base leading-6 text-gray-900 ml-2">
                {{__('Publish Date')}}:
            </p>

            <div class="text-sm mt-3 leading-5 text-gray-500">
                {{$order->created_at->toDateString()}}
            </div>
        </div>
    </div>
</x-layouts.app>



