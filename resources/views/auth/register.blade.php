<x-layouts.base>
    @section('content')
        <div class="p-10">
    <x-jet-authentication-card>
        <x-slot name="logo">
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <div>
                <div>
                    <x-jet-label for="first_name" value="{{ __('Name').' '.__('First') }}"/>
                    <x-jet-input id="first_name" class="block mt-1 w-full" type="text" name="first_name" :value="old('first_name')"
                                 required autofocus autocomplete="first_name"/>
                </div>
                <div>
                    <x-jet-label for="father_name" value="{{ __('name').' '.__('Father')}}"/>
                    <x-jet-input id="father_name" class="block mt-1 w-full" type="text" name="father_name" :value="old('father_name')"
                                 required autofocus autocomplete="name"/>
                </div>
                <div>
                    <x-jet-label for="grandfather_name" value="{{ __('name').' '.__('Grandfather')}}"/>
                    <x-jet-input id="grandfather_name" class="block mt-1 w-full" type="text" name="grandfather_name" :value="old('grandfather_name')"
                                 required autofocus autocomplete="name"/>
                </div>
                <div>
                    <x-jet-label for="family_name" value="{{ __('name').' '.__('Family')}}"/>
                    <x-jet-input id="family_name" class="block mt-1 w-full" type="text" name="family_name" :value="old('family_name')"
                                 required autofocus autocomplete="name"/>
                </div>
            </div>

            <div class="mt-4">
                <x-jet-label for="id_number" value="{{__('Number').' '. __('ID')}}" />
                <x-jet-input id="id_number" class="block mt-1 w-full" type="number" name="id_number" :value="old('id_number')" required />
            </div>
            <div class="mt-4">
                <x-jet-label for="card_issuer" value="{{__('Issuer').' '.__('Card')}}" />
                <x-jet-input id="card_issuer" class="block mt-1 w-full" type="text" name="card_issuer" :value="old('card_issuer')" required />
            </div>
            <div class="mt-4">
                <x-jet-label for="date" value="{{__('Date').' '. __('Issue')}}" />
                <x-jet-input id="date" class="block mt-1 w-full" type="Date" name="date" :value="old('date')" required />
            </div>
            <div class="mt-4">
                <x-jet-label for="reasons" value="{{ __('Reasons For Registration')}}" />
                <x-jet-input id="reasons" class="block mt-1 w-full" type="text" name="reasons" :value="old('reasons')" required />
            </div>
            <div class="mt-4">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
            </div>
            <div class="mt-4">
                <x-jet-label for="phone" value="{{ __('Phone') }}" />
                <x-jet-input id="phone" class="block mt-1 w-full" type="tel" name="phone" :value="old('phone')" required />
            </div>
            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('Password') }}" />
                <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" />
            </div>

            <div class="mt-4">
                <x-jet-label for="password_confirmation" value="{{ __('Confirm Password') }}" />
                <x-jet-input id="password_confirmation" class="block mt-1 w-full" type="password" name="password_confirmation" required autocomplete="new-password" />
            </div>

            <div class="flex items-center justify-end mt-4">
                <a class="underline bottom-0 left-0 text-sm text-gray-600 trxt-xl hover:text-gray-900 p-5" href="/posts/alshrot">
                    {{ __('Conditions') }}
                </a>
                <a class="underline text-sm text-gray-600 trxt-xl hover:text-gray-900 p-5" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <x-jet-button class="p-10">
                    {{ __('Register') }}
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
        </div>
        @endsection
    </x-layouts.base>
