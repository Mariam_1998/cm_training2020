<x-layouts.admin>
    <x-feedback/>
    <h1 class="mb-10 text-center text-xl">{{__('Edit').' '.__('link'). ": $link->title"}}</h1>

    <x-form action="{{ route('links.update',['link'=>$link->id])}}" method="PUT">
        @csrf
        <div class="m-3">

            <div class="mb-6 text-right">
                <label>{{__('Title')}}</label>
                <x-input type="text" value="{{$link->title}}" name="title"/>
            </div>

            <div class="mb-6 text-right">
                <label>{{__('URL')}}</label>
                <x-input type="url" value="{{$link->URL}}" name="URL"/>
            </div>

            <div class="mt-5">
                <button class="btn" type="submit">{{__('Edit')}}</button>
                <input type="button"
                       class="btn-cancel"
                       name="cancel" value="{{__('Cancel')}}" onClick="window.location.replace('/links/manage');"/>
            </div>
        </div>
    </x-form>

</x-layouts.admin>
