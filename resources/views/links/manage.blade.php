<x-manage model="Links" action="links/create" :data="$links" modelName="Link">
    <div class="w-full mx-5">
        <livewire:links-datatable/>
    </div>
</x-manage>
