<x-layouts.admin>
    <div>
        <x-feedback/>

        <h1>{{__('Create').' '.__('link')}}</h1>

        <x-form action="{{url('links')}}" method="post">
            @csrf
            <div class="m-4">

                <div class="mb-6 text-right">
                    <label>{{__('Title')}}</label>
                    <x-input type="text" name="title"/>
                </div>

                <div class="mb-6 text-right">
                    <label>{{__('URL')}}</label>
                    <x-input type="url" name="URL"/>
                </div>

                <div class="mt-5">
                    <button class="btn">{{__('Create')}}</button>

                    <input type="button"
                           class="btn-cancel"
                           name="cancel" value="{{__('Cancel')}}" onClick="window.location.replace('/links/manage');"/>
                </div>
            </div>
        </x-form>
    </div>
</x-layouts.admin>
