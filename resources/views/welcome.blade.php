@extends('components.layouts.app')

@section('content')
    <h1 class="w-full my-2 text-5xl font-bold leading-tight text-center text-gray-800">
        {{__('articles')}}</h1>
    <div class="w-full mb-4">
        <div class="h-1 mx-auto gradient w-64 opacity-25 my-0 py-0 rounded-t bg-purple-900"></div>
    </div>
    <div class="mx-auto">
        <div class="lg:m-10">
            @foreach( $posts as $post)
                <div class=" rounded mb-3">
                    <x-lastpost :post="$post"/>
                </div>
            @endforeach
        </div>
        <div class="flex justify-center my-1">
            <snap >{{ $posts->links()}}</snap>
        </div>
    </div>
    @endsection