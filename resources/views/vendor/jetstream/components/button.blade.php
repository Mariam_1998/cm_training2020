<button class="text-center flex-shrink-0 but-color text-white text-base font-semibold py-2 px-4 rounded-lg shadow-md"
        {{ $attributes->merge(['type' => 'submit', 'class' => 'inline-flex items-center px-4 py-2 bg-green-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-green-700 active:bg-green-900 focus:outline-none focus:border-green-900 focus:shadow-outline-green disabled:opacity-25 transition ease-in-out duration-150']) }}>
    {{ $slot }}
</button>
