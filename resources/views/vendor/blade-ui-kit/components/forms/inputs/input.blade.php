<input
        {{$attributes->merge(['class'=>"form-input mt-1 block w-full
            shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight
            my-4"]) }}
        name="{{ $name }}"
        type="{{ $type }}"
        id="{{ $id }}"
        @if($value)value="{{ $value }}"@endif
/>
