<x-layouts.admin>
    <x-feedback/>

    <h1>{{__('Create').' '.__('category')}}</h1>

    <div class="mx-3 text-right">

        <x-form action="{{url('categories')}}" method="post">
            @csrf
            <div class="bg-white shadow-lg rounded px-8 pt-6 pb-8 mb-4 flex flex-col my-2">

                <div class="-mx-3 md:flex mb-6">
                    <div class="md:w-1/2 px-3 mb-3 md:mb-0">
                        <label class="mb-3">{{__('name').' '.__('Category')}}</label>
                        <x-input name="name" value="{{ old('name') }}" placeholder="{{__('Name')}}"/>
                    </div>
                    <div class="md:w-1/2 px-3">
                        <label class="mb-3">{{__('type').' '.__('Category')}}</label>
                        <x-select class="ml-4 select" name="type" id="type"
                                  label="{{__('type').' '.__('Post')}}">
                            <option value="1">{{__('post')}}</option>
                            <option value="2">{{__('project')}}</option>
                        </x-select>
                    </div>
                </div>

                <label>{{__('description').' '.__('Category')}}</label>
                <x-summernote name="description" default=""/>
                <div class="mt-3">
                    <button class="btn mt-3" type="submit">{{__('Create')}}</button>

                    <input type="button" class="btn-cancel mt-3"
                           name="cancel" value="{{__('Cancel')}}"
                           onClick="window.location.replace('/categories/manage');"/>
                </div>
            </div>
        </x-form>

    </div>
</x-layouts.admin>
