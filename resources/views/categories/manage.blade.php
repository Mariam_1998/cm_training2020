<x-manage model="Categories" action="categories/create" :data="$categories" modelName="Category">
    <div class="w-full mx-5">
        <livewire:categories-datatable/>
    </div>

</x-manage>
