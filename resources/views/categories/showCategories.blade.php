@extends('components.layouts.app')

@section('content')

    <h1 class="w-full my-2 text-5xl font-bold leading-tight text-center text-gray-800">
        {{$category->name}}</h1>
    <div class="w-full mb-4">
        <div class="h-1 mx-auto gradient w-64 opacity-25 my-0 py-0 rounded-t bg-green-900"></div>
    </div>


    <div class="mx-auto ">
        <div class="lg:m-10 md:grid md:gap-6 md:grid-cols-3 m-5">
            @foreach( $projects as $project)
                <a href="/categories/{{$project->slug}}">
                    <div class="bg-white p-5 rounded-lg mt-6 border-green-600
                    lg:w-96 w-full border-t-8 border-green-600 rounded-lg flex justify-center items-center">
                        <div class="rounded mb-3">
                            <h4>{{$project->name}}</h4>
                        </div>
                    </div>
                </a>
            @endforeach
        </div>

    </div>

@endsection

