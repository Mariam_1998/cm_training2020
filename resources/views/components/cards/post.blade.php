<div class="m-10">
    <a href="/posts/{{$post->slug}}" class="flex flex-wrap no-underline hover:no-underline bg-white">

        <div class="w-full md:w-2/3 rounded-t" style="max-width: 300px">
            @if($post->hasMedia('posts') )
                @foreach($post->getMedia('posts') as $attachment)
                    {{$attachment}}
                @endforeach
            @else
                <img  src="{{asset('img/logo.jpg')}}" alt="Post">
            @endif
        </div>

        <div class="w-full md:w-1/3 flex flex-col  flex-grow flex-shrink text-center mt-5 ml-5">
            <div class="flex-1 rounded-t rounded-b-none overflow-hidden">
                <h1>{{$post->title}}</h1>
                <p class="text-justify  mt-3 text-base leading-6 text-gray-500 lg:pb-0 pb-3">
                    {{$post->short_description}}
                </p>

            </div>
        </div>

    </a>
</div>