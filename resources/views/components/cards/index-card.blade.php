<div class="pt-5 mb-3 flex items-center justify-center ">
    <div class="w-full mx-auto rounded-lg bg-white shadow-lg px-1 pt-3 pb-5 text-gray-800 text-center  bg-white" style="max-width: 480px">
        {{$slot}}
    </div>
</div>
