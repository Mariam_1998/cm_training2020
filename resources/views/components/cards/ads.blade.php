
@if(count($ads))
    <h1 class="pt-5">{{__('Ads')}}</h1>
    @foreach($ads as $ad)
        <a href="/posts/{{$ad->slug}}">
        <section style="margin-top:30px;">
            <div class="w-full rounded-t" style="max-width:500px">
                @if($ad->hasMedia('posts') )
                @foreach($ad->getMedia('posts') as $attachment)
                {{$attachment}}
                @endforeach
                @else
                <img  src="{{asset('img/logo.jpg')}}" alt="Post">
                @endif
            </div>
        </section>
        </a>
    @endforeach
@endif
