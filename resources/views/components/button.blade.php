<button class="bg-green-600 text-white text-base
    font-semibold py-2 px-4 rounded-lg shadow-md hover:bg-green-700
    focus:outline-none focus:ring-2 focus:ring-green-500
    focus:ring-offset-2 focus:ring-offset-green-200">
    {{$slot}}
</button>
