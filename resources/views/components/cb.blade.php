<div class="form-group ml-4">
    <input name="{{$name}}" type="hidden" value="0">
    <input  type="checkbox" id="{{$name}}" name="{{$name}}" value="1" {{ old("$name", $default) ? 'checked' : '' }}>
    <label>{{__("$label")}} </label>
</div>
