<div class="md:flex flex-col md:flex-row md:min-h-screen w-full">
    <div class="flex flex-col w-full md:w-64 text-gray-700 bg-white dark-mode:text-gray-200 dark-mode:bg-gray-800 flex-shrink-0">
        <nav class="flex-grow md:block px-4 pb-4 md:pb-0 md:overflow-y-auto">
            <a class="sidebar" href="/users/manage">{{__("Users")}}</a>
            <a class="sidebar" href="/donations/manage">{{__("Donations")}}</a>
            <a class="sidebar" href="/orders/manage">{{__("Orders")}}</a>
            <a class="sidebar" href="/posts/manage">{{__("Posts")}}</a>
            <a class="sidebar" href="/categories/manage">{{__("Categories")}}</a>
            <a class="sidebar" href="/links/manage">{{__('Links')}}</a>
            <a class="sidebar" href="/albums/manage">{{__('Albums')}}</a>
        </nav>
    </div>
</div>
