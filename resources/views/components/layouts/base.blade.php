<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="apple-touch-icon" sizes="180x180" href="./apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="./favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="./favicon-16x16.png">
    <link rel="manifest" href="./site.webmanifest">
    <link rel="mask-icon" href="./safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description"
          content="{{ $description ?? 'Cube Master' }}">
    <meta property="og:site_name" content="{{ config('app.name') }}"/>
    <meta property="og:title" content="{{ isset($title) ? strip_tags($title) . ' | ' : '' }}{{ config('app.name') }}"/>
    <meta property="og:description"
          content="{{ $description ?? config('app.name') }}"/>
    <meta property="og:url" content="{{ request()->url() }}"/>
    <meta property="og:image" content="{{ $social_image ?? 'https://cm.codes/img/logo.svg' }}"/>
    <meta property="og:type" content="website"/>
    <meta name="twitter:card" content="{{ $social_size ?? 'summary' }}">
    <link rel="home" href="{{ config('app.url') }}">
    <link rel="icon" href="/favicon.ico">

    @stack('meta')
    {!! SEOMeta::generate() !!}
    <title>{{ $title?? config('app.name') }}</title>

    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    @stack('styles')
    @livewireStyles

    @section('header')
        <x-layouts.header/>
    @endsection
</head>

<body
        dir="{{app()->getLocale()=='ar'?'rtl':'ltr'}}"
      class="min-h-screen bg-gray-50" id="wrapper"
>
@yield('header')

@yield('sidebar')

@yield('content')

<x-layouts._footer/>

@stack('modals')

</body>
</html>

