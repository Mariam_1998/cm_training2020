<footer class="footer footer-color bg-no-repeat bg-bottom md:bg-top  pt-5 pb-2 mt-5 border-t border-gray-200">
    {{--<div class="float-left ml-5">--}}
    {{--<x-scrolltop/>--}}
    {{--</div>--}}
    <ul class="flex-col mr-10 ml-5 list-none md:flex-row flex justify-around">
        <li class="md:mr-2 flex flex-col">
            <p class="mb-2 text-gray-900  text-base text-xl mb-5">{{__('alber')}}</p>
            <p class="mb-2 text-white text-base">
                الجمعية من أوائل الجمعيات الخيرية في العالم الإسلامي التي أحيت <br>
                سنة المصطفى صلى الله عليه وسلم في إنشاء المشاريع الوقفية الصغيرة.<br>
                مع التركيز في عملها على المشاريع التنموية التي تحقق لاستدامة، وتهدف<br>
                الجمعية للقيام بأعمال التنمية للمجتمعات الأقل حظاً مستهدفة بذلك الفئات<br>
                الاجتماعية الأكثر احتياجاً والمرضى والأيتام ومنكوبي الكوارث والمجاعات<br>
                والقيام بكافة أنشطة البر والخير.</p>

        </li>
        <li class="md:mr-2 flex flex-col">
            <p class="mb-2 text-gray-900  text-base text-xl mb-5">{{__('Categories')}}</p>
            @foreach($categories as $category)
                <a href="/categories/{{$category->slug}}" class="mb-2 text-white hover:text-green-200 text-lg ml-3">
                    {{$category->name}}
                </a>
            @endforeach
        </li>

        <li class="md:mr-2 flex flex-col">
            <p class="mb-2 text-gray-900  text-base text-xl mb-5">{{__('Links')}}</p>
            @foreach($links as $link)
                <a href="{{$link->URL}}" class="text-white mb-2 hover:text-green-200 text-lg ml-3">
                    {{$link->title}}
                </a>
            @endforeach
        </li>
        <li class="md:mr-2 ">
            <div class="flex-grow ml-10 md:ml-5">

                <div>
                    <a href="https://cm.codes" target="_blank">
                        <img class="h-36 mx-auto mt-8" src="{{asset('img/cm-logo.png')}}" alt="">
                    </a>
                </div>
            </div>

        </li>

    </ul>

    <div class="flex justify-center text-white mb-3 mt-3">
        &copy; <a href="{{url('/')}}" class="text-white" title="{{config('app.name')}}">{{config('app.name')}}</a> {{ date('Y') }}.
    </div>
</footer>

<script src="{{ mix('js/app.js') }}"></script>


@livewireScripts
@stack('scripts')
