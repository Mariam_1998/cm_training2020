<div class="md:flex flex-col md:flex-row md:min-h-screen w-full">
    <div class="flex flex-col w-full md:w-64 text-gray-700 bg-white dark-mode:text-gray-200 dark-mode:bg-gray-800 flex-shrink-0">
        <nav class="flex-grow md:block px-4 mb-5 pb-4 md:pb-0 md:overflow-y-auto">
            <x-cards.ads :ads="$ads"/>
        </nav>
        <hr>
        <div class="mt-10 lg:mr-0 mr-10">
        <a class="twitter-timeline"
           href="https://twitter.com/Ber_Msc1?ref_src=twsrc%5Etfw"
           data-tweet-limit="3"
           data-width="300"
           data-height="300">
            Tweets by Ber_Msc1</a>
        <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
        </div>
    </div>
</div>
