<nav class="relative flex flex-wrap items-center justify-between px-2 py-3 navbar-expand-lg bg-white">
    <div class="container lg:px-4 mx-auto flex flex-wrap items-center justify-between">
        <div class="w-full relative flex justify-between lg:w-auto  px-4 lg:static lg:block lg:justify-start">
            <a href="/home">
                <span class="sr-only">{{__('Alber')}}</span>
                <img class="w-32" src="{{asset('img/logo.jpg')}}" alt="logo">
            </a>
            <button class="text-gray-500 cursor-pointer text-xl leading-none px-3 py-1 border border-solid border-transparent rounded bg-transparent block lg:hidden outline-none focus:outline-none"
                    type="button" onclick="toggleNavbar('example-collapse-navbar')">
                <svg fill="currentColor" viewBox="0 0 20 20" class="w-6 h-6">
                    <path fill-rule="evenodd"
                          d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM9 15a1 1 0 011-1h6a1 1 0 110 2h-6a1 1 0 01-1-1z"
                          clip-rule="evenodd"></path>
                </svg>
            </button>
        </div>
        <div class="w-full flex-grow lg:flex lg:items-center lg:w-auto hidden
                lg:mt-0 lg:bg-transparent text-black lg:p-0 z-20"
             id="example-collapse-navbar">
            <ul class="flex flex-col lg:flex-row list-none lg:ml-auto">
                <li class="nav-item">
                    <a class="links" href="/donations/create">{{ __('Donation') }}</a>
                </li>
                <li class="nav-item">
                    <a class="links" href="/posts">{{ __('Posts') }}</a>
                </li>
                <li class="nav-item">
                    <a class="links" href="/categories/{{__('Projects')}}">{{ __('Projects') }}</a>
                </li>
                <hr>
                @guest
                    @if (Route::has('login'))
                        <li class="nav-item">
                            <a class="links"
                               href="{{ route('login') }}">
                                <span class="ml-2">{{ __('Login') }}</span>
                            </a>
                        </li>
                    @endif
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="links"
                               href="{{ route('register') }}">
                                <span class="ml-2">{{ __('Register') }}</span>
                            </a>
                        </li>
                    @endif
                @else
                    @if(Auth::check())
                        <li class="nav-item">
                            <a class="links"
                               href="/orders/create">
                                <span class="ml-2">{{__('Create').' '. __('order')}}</span>
                            </a>
                        </li>
                    @endif
                    @if(Auth::check() && Auth::user()->is_admin)
                        <li class="nav-item">
                            <a class="links"
                               href="/orders/manage">
                                <span class="ml-2">{{__('Manage')}}</span>
                            </a>
                        </li>
                    @endif

                    <li class="nav-item">
                        <a class="links"
                           href="/user/profile">
                            <span class="ml-2">{{__('My Profile')}}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="links"
                           href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                            <span class="ml-2">{{ __('Logout') }}</span>
                        </a>
                    </li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                @endguest
            </ul>
        </div>
    </div>
</nav>
<script>
    function toggleNavbar(collapseID) {
        document.getElementById(collapseID).classList.toggle("hidden");
        document.getElementById(collapseID).classList.toggle("flex");
    }
</script>