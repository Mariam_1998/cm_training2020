    <label class="inline-flex items-center">
        <input type="radio" class="form-radio" name="{{$name}}" value="{{$value}}">
        <span class="ml-2">{{$label??$name}}</span>
    </label>
