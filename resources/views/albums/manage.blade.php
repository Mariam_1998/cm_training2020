<x-manage model="Albums" action="albums/create" :data="$albums" modelName="Album">
    <div class="w-full mx-5">
        <livewire:albums-datatable/>
    </div>

</x-manage>
