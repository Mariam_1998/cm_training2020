<x-layouts.admin>
    <x-feedback/>

    <h1>{{__('Create').' '.__('album')}}</h1>

    <div class="mx-3 text-right">

        <x-form action="{{url('albums')}}" method="post" has-files>
            @csrf
            <div class="bg-white shadow-lg rounded px-8 pt-6 pb-8 mb-4 flex flex-col my-2">

                <div class="-mx-3 md:flex mb-6">
                    <div class="md:w-1/2 px-3 mb-3 md:mb-0">
                        <label class="mb-3">{{__('title').' '.__('Album')}}</label>
                        <x-input name="title" value="{{ old('title') }}" placeholder="{{__('title')}}"/>
                    </div>
                </div>

                <div class="field mb-5">
                    <label class="text-lg font-extrabold leading-8 tracking-tight text-gray-800 sm:text-lg sm:leading-9"
                           for="image">{{__('Add').' '.__('Images').' '.__('Album')}}</label>
                    <div class="mt-3">
                        <x-input class="input" type="file" id="image" name="image" required/>
                    </div>
                </div>
                <div class="mt-3">
                    <button type="submit" class="btn">
                        {{__('Create')}}
                    </button>
                    <input type="button" class="btn-cancel mt-3"
                           name="cancel" value="{{__('Cancel')}}" onClick="window.location.replace('/albums/manage');"/>
                </div>
            </div>
        </x-form>
    </div>
</x-layouts.admin>
