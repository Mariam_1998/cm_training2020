@extends('components.layouts.app')
@section('content')
    <div class="m-5">
        <div class="mt-8 p-4">
            <div>
                <x-form action="{{ route('users.update',['user'=>$user->id]) }}" method="PUT">
                    @csrf
                    <div class="bg-white shadow-lg rounded px-8 pt-6 pb-8 mb-4 flex flex-col my-2">
                    <div class="flex flex-col md:flex-row">
                        <div class="w-full mx-2 flex-1 svelte-1l8159u">
                            <label>{{ __('Name').' '.__('First') }}</label>
                            <div class="divUser">
                                <input name="first_name" value="{{$user->first_name}}" class="input"></div>
                        </div>
                        <div class="w-full mx-2 flex-1 svelte-1l8159u">
                            <label>{{ __('name').' '.__('Father')}}</label>
                            <div class="divUser">
                                <input name="father_name" value="{{$user->father_name}}" class="input"></div>
                        </div>
                        <div class="w-full mx-2 flex-1 svelte-1l8159u">
                            <label>{{ __('name').' '.__('Grandfather')}}</label>
                            <div class="divUser">
                                <input name="grandfather_name" value="{{$user->grandfather_name}}" class="input"></div>
                        </div>
                        <div class="w-full mx-2 flex-1 svelte-1l8159u">
                            <label>{{ __('name').' '.__('Family')}}</label>
                            <div class="divUser">
                                <input name="family_name" value="{{$user->family_name}}" class="input"></div>
                        </div>
                    </div>

                    <div class="flex flex-col md:flex-row">
                        <div class="w-full mx-2 flex-1 svelte-1l8159u">
                            <label>{{__('Number').' '. __('ID')}}</label>
                            <div class="divUser">
                                <input name="id_number" value="{{$user->id_number}}" class="input"></div>
                        </div>
                        <div class="w-full mx-2 flex-1 svelte-1l8159u">
                            <label>{{__('Issuer').' '.__('Card')}}</label>
                            <div class="divUser">
                                <input name="card_issuer" value="{{$user->card_issuer}}" class="input"></div>
                        </div>
                        <div class="w-full mx-2 flex-1 svelte-1l8159u">
                            <label>{{__('Date').' '. __('Issue')}}</label>
                            <div class="divUser">
                                <input name="date" value="{{$user->date}}" class="input"></div>
                        </div>
                    </div>
                    <div class="flex flex-col md:flex-row">
                        <div class="w-full mx-2 flex-1 svelte-1l8159u">
                            <label>{{ __('Reasons For Registration')}}</label>
                            <div class="divUser">
                                <input name="reasons" value="{{$user->reasons}}" class="input"></div>
                        </div>
                        <div class="w-full mx-2 flex-1 svelte-1l8159u">
                            <label>{{__('Email')}}</label>
                            <div class="divUser">
                                <input name="email" value="{{$user->email}}" class="input"></div>
                        </div>
                        <div class="w-full mx-2 flex-1 svelte-1l8159u">
                            <label>{{__('Phone')}}</label>
                            <div class="divUser">
                                <input name="phone" value="{{$user->phone}}" class="input"></div>
                        </div>
                    </div>
                    <div class="flex my-5">
                        <x-cb name="is_admin" label="Accept user as an admin" :default="$user->is_admin"/>
                    </div>
                    <div class="flex p-2 mt-4">
                        <button type="Submit" class="btn ml-2">{{__('Save')}}</button>
                        <input type="button" class="btn-cancel"
                               name="cancel" value="{{__('Cancel')}}" onClick="window.location.replace('/users/manage')"/>
                    </div>
                    </div>
                </x-form>
            </div>
        </div>
    </div>

@endsection
