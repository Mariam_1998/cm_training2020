@extends('components.layouts.app')
@section('content')
    <div class="m-5 print-area">
        <div class="mt-8 p-4">
            <div>
                <div class="bg-white shadow-lg rounded px-8 pt-6 pb-8 mb-4 flex flex-col my-2">
                    <div class="flex flex-col md:flex-row">
                        <div class="w-full mx-2 flex-1 svelte-1l8159u">
                            <label>{{ __('Name').' '.__('First') }}</label>
                            <div class="divUser">
                                <input name="first_name" value="{{$user->first_name}}" class="input bg-gray-50"
                                       readonly></div>
                        </div>
                        <div class="w-full mx-2 flex-1 svelte-1l8159u">
                            <label>{{ __('name').' '.__('Father')}}</label>
                            <div class="divUser">
                                <input name="father_name" value="{{$user->father_name}}" class="input bg-gray-50"
                                       readonly></div>
                        </div>
                        <div class="w-full mx-2 flex-1 svelte-1l8159u">
                            <label>{{ __('name').' '.__('Grandfather')}}</label>
                            <div class="divUser">
                                <input name="grandfather_name" value="{{$user->grandfather_name}}"
                                       class="input bg-gray-50" readonly></div>
                        </div>
                        <div class="w-full mx-2 flex-1 svelte-1l8159u">
                            <label>{{ __('name').' '.__('Family')}}</label>
                            <div class="divUser">
                                <input name="family_name" value="{{$user->family_name}}" class="input bg-gray-50"
                                       readonly></div>
                        </div>
                    </div>

                    <div class="flex flex-col md:flex-row">
                        <div class="w-full mx-2 flex-1 svelte-1l8159u">
                            <label>{{__('Number').' '. __('ID')}}</label>
                            <div class="divUser">
                                <input name="id_number" value="{{$user->id_number}}" class="input bg-gray-50" readonly>
                            </div>
                        </div>
                        <div class="w-full mx-2 flex-1 svelte-1l8159u">
                            <label>{{__('Issuer').' '.__('Card')}}</label>
                            <div class="divUser">
                                <input name="card_issuer" value="{{$user->card_issuer}}" class="input bg-gray-50"
                                       readonly></div>
                        </div>
                        <div class="w-full mx-2 flex-1 svelte-1l8159u">
                            <label>{{__('Date').' '. __('Issue')}}</label>
                            <div class="divUser">
                                <input name="date" value="{{$user->date}}" class="input bg-gray-50" readonly></div>
                        </div>
                    </div>
                    <div class="flex flex-col md:flex-row">
                        <div class="w-full mx-2 flex-1 svelte-1l8159u">
                            <label>{{ __('Reasons For Registration')}}</label>
                            <div class="divUser">
                                <input name="reasons" value="{{$user->reasons}}" class="input bg-gray-50" readonly>
                            </div>
                        </div>
                        <div class="w-full mx-2 flex-1 svelte-1l8159u">
                            <label>{{__('Email')}}</label>
                            <div class="divUser">
                                <input name="email" value="{{$user->email}}" class="input bg-gray-50" readonly></div>
                        </div>
                        <div class="w-full mx-2 flex-1 svelte-1l8159u">
                            <label>{{__('Phone')}}</label>
                            <div class="divUser">
                                <input name="phone" value="{{$user->phone}}" class="input bg-gray-50" readonly></div>
                        </div>
                    </div>
                    <br>
                    @if($orders)
                        <div class="container pt-5">
                            <h1 class="text-2xl">{{__('orders').' '.__('User')}}</h1>
                            <table class="w-full flex flex-row flex-no-wrap sm:bg-white rounded-lg overflow-hidden sm:shadow-lg my-5">
                                <thead class="text-white bg-green-500">
                                @foreach($orders as $order)
                                    <tr class="bg-teal-400 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none sm:mb-0">
                                        <th class="p-3 text-center lg:text-base text-xs">{{__('Title')}}</th>
                                        <th class="p-3 text-center lg:text-base text-xs">{{__('Problem')}}</th>
                                        <th class="p-3 text-center lg:text-base text-xs">{{__('Device')}}</th>
                                        <th class="p-3 text-center lg:text-base text-xs">{{__('Type')}}</th>
                                        <th class="p-3 text-center lg:text-base text-xs">{{__('Address')}}</th>
                                        <th class="p-3 text-center lg:text-base text-xs">{{__('State')}}</th>
                                        <th class="p-3 text-center lg:text-base text-xs">{{__('Notes')}}</th>
                                        <th class="p-3 text-center lg:text-base text-xs">{{__('Date')}}</th>
                                    </tr>
                                @endforeach
                                </thead>
                                <tbody class="flex-1 sm:flex-none">
                                @foreach($orders as $order)
                                    <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                                        <td class="border-grey-light border hover:bg-gray-100 p-3 lg:text-base text-xs">{{$order->title}}</td>
                                        <td class="border-grey-light border hover:bg-gray-100 p-3 lg:text-base text-xs">{{$order->problem}}</td>
                                        <td class="border-grey-light border hover:bg-gray-100 p-3 lg:text-base text-xs">{{$order->device}}</td>
                                        <td class="border-grey-light border hover:bg-gray-100 p-3 lg:text-base text-xs">{{__('order_'.$order->type)}}</td>
                                        <td class="border-grey-light border hover:bg-gray-100 p-3 lg:text-base text-xs">{{$order->address}}</td>
                                        <td class="border-grey-light border hover:bg-gray-100 p-3 lg:text-base text-xs">{{__('state_'.$order->state)}}</td>
                                        <td class="border-grey-light border hover:bg-gray-100 p-3 lg:text-base text-xs">{{$order->notes}}</td>
                                        <td class="border-grey-light border hover:bg-gray-100 p-3 lg:text-base text-xs">{{$order->created_at->toDateString()}}</td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="flex justify-end mb-5 flex-col md:flex-row mb0 lg:ml-5 mr-5">
            <a class="btn inline-flex justify-center items-center w-40" onclick="myFunction()">
                <svg class="w-6 h-6 ml-2" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                     class="feather feather-plus-square">
                    <rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect>
                    <line x1="12" y1="8" x2="12" y2="16"></line>
                    <line x1="8" y1="12" x2="16" y2="12"></line>
                </svg>
                <span>{{__('Print')}}</span>
            </a>
        </div>
    </div>

@endsection
<script>
    function myFunction() {
        window.print();
    }
</script>