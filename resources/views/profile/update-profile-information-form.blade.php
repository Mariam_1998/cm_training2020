<x-jet-form-section submit="updateProfileInformation">
    <x-feedback/>
    <x-slot name="title">
        {{ __('Profile Information') }}
    </x-slot>

    <x-slot name="description">
        {{ __('Update your account\'s profile information and email address.') }}
    </x-slot>

    <x-slot name="form">
        <!-- Profile Photo -->
        @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
            <div x-data="{photoName: null, photoPreview: null}" class="col-span-6 sm:col-span-4">
                <!-- Profile Photo File Input -->
                <input type="file" class="hidden"
                            wire:model="photo"
                            x-ref="photo"
                            x-on:change="
                                    photoName = $refs.photo.files[0].name;
                                    const reader = new FileReader();
                                    reader.onload = (e) => {
                                        photoPreview = e.target.result;
                                    };
                                    reader.readAsDataURL($refs.photo.files[0]);
                            " />

                <x-jet-label for="photo" value="{{ __('Photo') }}" />

                <!-- Current Profile Photo -->
                <div class="mt-2" x-show="! photoPreview">
                    <img src="{{ $this->user->profile_photo_url }}" alt="{{ $this->user->name }}" class="rounded-full h-20 w-20 object-cover">
                </div>

                <!-- New Profile Photo Preview -->
                <div class="mt-2" x-show="photoPreview">
                    <span class="block rounded-full w-20 h-20"
                          x-bind:style="'background-size: cover; background-repeat: no-repeat; background-position: center center; background-image: url(\'' + photoPreview + '\');'">
                    </span>
                </div>

                <x-jet-secondary-button class="mt-2 mr-2" type="button" x-on:click.prevent="$refs.photo.click()">
                    {{ __('Select A New Photo') }}
                </x-jet-secondary-button>

                @if ($this->user->profile_photo_path)
                    <x-jet-secondary-button type="button" class="mt-2" wire:click="deleteProfilePhoto">
                        {{ __('Remove Photo') }}
                    </x-jet-secondary-button>
                @endif

                <x-jet-input-error for="photo" class="mt-2" />
            </div>
        @endif

        <!-- First Name -->
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="first_name" value="{{ __('Name').' '.__('First') }}" />
            <x-jet-input id="first_name" type="text" class="mt-1 block w-full" wire:model.defer="state.first_name" autocomplete="first_name" />
            <x-jet-input-error for="first_name" class="mt-2" />
        </div>
        <!-- Father Name -->
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="father_name" value="{{ __('name').' '.__('Father')}}" />
            <x-jet-input id="father_name" type="text" class="mt-1 block w-full" wire:model.defer="state.father_name" autocomplete="father_name" />
            <x-jet-input-error for="father_name" class="mt-2" />
        </div>
        <!-- First Name -->
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="grandfather_name" value="{{ __('name').' '.__('Grandfather')}}" />
            <x-jet-input id="grandfather_name" type="text" class="mt-1 block w-full" wire:model.defer="state.grandfather_name" autocomplete="grandfather_name" />
            <x-jet-input-error for="grandfather_name" class="mt-2" />
        </div>
        <!-- First Name -->
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="family_name" value="{{ __('name').' '.__('Family')}}" />
            <x-jet-input id="family_name" type="text" class="mt-1 block w-full" wire:model.defer="state.family_name" autocomplete="family_name" />
            <x-jet-input-error for="family_name" class="mt-2" />
        </div>
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="id_number" value="{{__('Number').' '. __('ID')}}" />
            <x-jet-input id="id_number" class="block mt-1 w-full text-right" type="number" name="id_number" wire:model.defer="state.id_number" />
        </div>
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="card_issuer" value="{{__('Issuer').' '.__('Card')}}" />
            <x-jet-input id="card_issuer" class="block mt-1 w-full text-right" type="text" name="card_issuer" wire:model.defer="state.card_issuer" />
        </div>
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="date" value="{{__('Date').' '. __('Issue')}}" />
            <x-jet-input id="date" class="block mt-1 w-full text-right" type="date" name="date" wire:model.defer="state.date" />
        </div>
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="reasons" value="{{ __('Reasons For Registration')}}" />
            <x-jet-input id="reasons" class="block mt-1 w-full text-right" type="text" name="reasons" wire:model.defer="state.reasons" />
        </div>
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="phone" value="{{ __('Phone') }}" />
            <x-jet-input id="phone" class="block mt-1 w-full text-right" type="tel" name="phone" wire:model.defer="state.phone"  autocomplete="phone" />
        </div>
        <!-- Email -->
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="email" value="{{ __('Email') }}" />
            <x-jet-input id="email" type="email" class="mt-1 block w-full" wire:model.defer="state.email" />
            <x-jet-input-error for="email" class="mt-2" />
        </div>
    </x-slot>

    <x-slot name="actions">
        <x-jet-action-message class="mr-3" on="saved">
            {{ __('Saved.') }}
        </x-jet-action-message>

        <x-jet-button wire:loading.attr="disabled" wire:target="photo">
            {{ __('Save') }}
        </x-jet-button>
    </x-slot>
</x-jet-form-section>
