<x-layouts.public>
    <x-feedback/>
        <div class="font-sans antialiased ">
            <!-- Top Navigation -->
            <div class="pt-3 mb-10 bg-white">
            <x-slider :albums="$albums"/>
            </div>
                <div class="mx-auto">
                    <div class="text-center w-full text-grey-darkest">
                        <div class="md:m-2 m-0">
                            @foreach( $featuredPosts as $featuredPost)
                                <div class="bg-white rounded-xl lg:m-10 mb-3">
                                    <x-cards.post :post="$featuredPost" />
                                </div>
                            @endforeach

                        </div>

                </div>
                </div>
            </div>

</x-layouts.public>
