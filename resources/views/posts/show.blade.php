<x-layouts.app>
    <div class="m-5 w-full">

        <div class="lg:p-6 pl-3">
            <div class="object-contain lg:w-80 md:w-72 ml-auto mr-auto block ">
                @foreach($post->getMedia('posts') as $attachment)
                    {{$attachment}}
                @endforeach
            </div>
            <h1 class="mt-3">{{$post->title}}</h1>
            <p class="mt-3 text-base leading-6 text-gray-500 pl-3 text-justify">
                {!!$post->body!!}
            </p>

            <div class="flex">
                <p class="mt-3 text-base leading-6 text-gray-900 ml-2">
                    {{__('Type')}}:
                </p>
                <p class="mt-3 text-base leading-6 text-green-800">
                    {{$post->category->name}}
                </p>
            </div>

            <div class="text-sm mt-3 leading-5 text-gray-500">
                {{__('Publish Date')}}: {{$post->created_at->toDateString()}}
            </div>

        </div>

    </div>
</x-layouts.app>



