@extends('components.layouts.app')

@section('content')
    <div class="mt-10">
        <h1 class="w-full my-2 text-5xl font-bold leading-tight text-center text-gray-800">
            {{__('articles')}}</h1>
        <div class="w-full mb-4">
            <div class="h-1 mx-auto gradient w-64 opacity-25 my-0 py-0 rounded-t bg-green-900"></div>
        </div>
        <div class="mx-auto">
            <div class="m-2">
                @foreach( $posts as $featuredPost)
                    <div class="bg-white rounded-xl lg:m-10 mb-3">
                        <x-cards.post :post="$featuredPost"/>
                    </div>
                @endforeach
            </div>
            <div class="flex justify-center my-1">
                <snap>{{ $posts->links()}}</snap>
            </div>
        </div>
    </div>
@endsection

