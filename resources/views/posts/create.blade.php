<x-layouts.admin>
    <x-feedback/>

    <h1>{{__('Create').' '.__('post')}}</h1>

    <x-form action="{{url('posts')}}" method="post" has-files>
        <div class="bg-white shadow-lg rounded px-8 pt-6 pb-8 mb-4 flex flex-col my-2">
            <div class="-mx-3 md:flex mb-6">
                <div class="md:w-1/2 px-3 mb-3 md:mb-0">
                    <label class="mb-3">{{__('Title')}}</label>
                    <x-input name="title" value="{{ old('title') }}" placeholder="{{__('Title')}}"/>
                </div>
                <div class="md:w-1/2 px-3">
                    <label class="mb-3">{{__('type').' '.__('Post')}}</label>
                    <x-select class="ml-4 select" name="category_id" id="category"
                              label="{{__('type').' '.__('Post')}}">
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </x-select>
                </div>
            </div>
            <label class="mb-3">{{__('description').' '.__('Post')}}</label>
            <x-summernote name="body" default=""/>

            <label class="mb-3">{{__('description').' '.__('Short')}}</label>
            <x-input name="short_description" value="{{ old('short_description') }}"
                     placeholder="{{__('description').' '.__('Short')}}"/>

            <div class="field mb-5">
                <label class="text-lg font-extrabold leading-8 tracking-tight text-gray-800 sm:text-lg sm:leading-9"
                       for="image">{{__('Add Main image for the Post')}}</label>
                <div class="mt-3">
                    <x-input class="input" type="file" id="image" name="image"/>
                </div>
            </div>

            <div class="flex mt-3">
                <x-cb name="is_published" label="Is Published" default=""/>
                <x-cb name="is_featured" label="Is Featured" default=""/>
            </div>
            <div class="mt-3">
                <button type="submit" class="btn">
                    {{__('Create')}}
                </button>
                <input type="button" class="btn-cancel mt-3"
                       name="cancel" value="{{__('Cancel')}}" onClick="window.location.replace('/posts/manage');"/>
            </div>
        </div>
    </x-form>


</x-layouts.admin>
