<x-layouts.admin>
    <x-feedback/>
    <h1>{{__('Edit').' '.__('post'). ": $post->title"}}</h1>
    <x-form action="{{ route('posts.update',['post'=>$post->slug]) }}" method="PUT" has-files>
        <div class="bg-white shadow-lg rounded px-8 pt-6 pb-8 mb-4 flex flex-col my-2">
            <div class="-mx-3 md:flex mb-6">
                <div class="md:w-1/2 px-3 mb-3 md:mb-0">
                    <label class="mb-3">{{__('Title')}}</label>
                    <x-input name="title" value="{{$post->title}}" placeholder="{{__('Title')}}"/>
                </div>
                <div class="md:w-1/2 px-3">
                    <label class="mb-3">{{__('type').' '.__('Post')}}</label>
                    <x-select class="ml-4 select" name="category_id" id="category"
                              label="{{__('type').' '.__('Post')}}">
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}" {{$category->id == $post->category_id  ? 'selected' : ''}}>
                                {{ $category->name }}</option>
                        @endforeach
                    </x-select>
                </div>
            </div>
            <label>{{__('description').' '.__('Post')}}</label>
            <x-summernote name="body" :default="$post->body"/>

            <label>{{__('description').' '.__('Short')}}</label>
            <x-input name="short_description" value="{{ $post->short_description }}"
                     placeholder="{{__('description').' '.__('Short')}}"/>

            <div class="field text-right ">
                <div>
                    @if($post->hasMedia('posts') )
                        <div class="object-cover mb-5">
                            @foreach($post->getMedia('posts') as $attachment)
                                <div class="h-25 w-25">
                                    {{$attachment}}
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>

                <div class="field mb-5">
                    <label for="image">{{__('Edit Main image for the Post')}}</label>
                    <div class="mt-3">
                        <x-input type="file" id="image" name="image"/>
                    </div>
                </div>
            </div>

            <div class="flex my-5">
                <x-cb name="is_published" label="Is Published" :default="$post->is_published"/>
                <x-cb name="is_featured" label="Is Featured" :default="$post->is_featured"/>
            </div>
            <div class="mt-3">
                <button type="submit" class="btn">
                    {{__('Edit')}}
                </button>
                <input type="button" class="btn-cancel"
                       name="cancel" value="{{__('Cancel')}}" onClick="window.location.replace('/posts/manage');"/>
            </div>
        </div>
    </x-form>

</x-layouts.admin>



