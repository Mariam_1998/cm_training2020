<x-manage model="Posts" action="posts/create" :data="$posts" modelName="Post">
    <div class="w-full mx-5">
        <livewire:posts-datatable/>
    </div>
</x-manage>
