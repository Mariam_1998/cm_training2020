<x-layouts.admin>
    <div class="drag-and-drop">
        <div class="drag-and-drop__container drag-and-drop__container--from">
            <h3 class="drag-and-drop__title">{{__('Order').' '.__('Posts')}}</h3>

            <ul>
                @foreach($posts as $post)
                <li>{{$post->order}}  {{$post->title}} <a class='up' href='#'>up</a> <a class='down' href='#'>down</a></li>
                @endforeach
                <li class="count">i <a class='up' href='#'>up</a> <a class='down' href='#'>down</a></li>
                <li>3 <a class='up' href='#'>up</a> <a class='down' href='#'>down</a></li>
                <li>4 <a class='up' href='#'>up</a> <a class='down' href='#'>down</a></li>
                <li>5 <a class='up' href='#'>up</a> <a class='down' href='#'>down</a></li>
                <li>6 <a class='up' href='#'>up</a> <a class='down' href='#'>down</a></li>
            </ul>

            {{--<ul class="drag-and-drop__items" id="list">--}}
                {{--<!-- loop through the items -->--}}
                {{--@foreach($posts as $post)--}}
                    {{--<li id="{{$post->id}}" class="li">--}}
                        {{--<a  class=" mt-8"--}}
                                {{--@click="up()">{{__('Up')}}</a>--}}
                        {{--<p>{{$post->title}}</p><p>{{$post->order}}</p>--}}
                        {{--<a  class=" mt-8"--}}
                                {{--@click="down()">{{__('Down')}}</a>--}}
                    {{--</li>--}}
                {{--@endforeach--}}
            {{--</ul>--}}
        </div>
    </div>
    <script>
        window.onload = function () {
            var upLink = document.querySelectorAll(".up");
            var count = 0;
            for (var i = 0; i < upLink.length; i++) {
                upLink[i].addEventListener('click', function () {
                    var wrapper = this.parentElement;


                    if (wrapper.previousElementSibling)
                        wrapper.parentNode.insertBefore(wrapper, wrapper.previousElementSibling);
                });
            }

            var downLink = document.querySelectorAll(".down");

            for (var i = 0; i < downLink.length; i++) {
                downLink[i].addEventListener('click', function () {
                    var wrapper = this.parentElement;

                    if (wrapper.nextElementSibling)
                        wrapper.parentNode.insertBefore(wrapper.nextElementSibling, wrapper);
                });
            }

            document.getElementsByClassName('count').innerHTML = 'it is '+count;
        }
    </script>

</x-layouts.admin>
