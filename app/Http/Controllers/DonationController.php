<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Category;
use App\Models\Donation;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Facades\SEOMeta;

class DonationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('auth')->except(['create','store']);
        $this->middleware('admin')->only(['manage','edit','update','delete']);
    }


    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function manage(Donation $donation)
    {
        return view('donations.manage', ['donations' => Donation::all()]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        SEOMeta::setTitle(__('Donations'));
        SEOMeta::addKeyword(['Alber','المنشورات','جمعية البر','ahsaber','مساعدات','مساعدة','محتاجين'
            ,'جمع التبرعات','خيرية','دعم','تبرع','جمعية','طريقة التبرع']);

        $projects = Category::where('type',2)->get();
        $accounts = Account::all();

        return view('donations.create', compact('projects','accounts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->validate([
            'payment_type' => 'required',
            'date' => 'required',
            'account_id' => 'required',
            'amount' => 'required',
            'from_account' => 'nullable',
            'name' => 'required',
            'phone' => 'required',
            'category_id' => 'required',
        ]);
        $donation = Donation::create($data);
        if ($request->hasFile('image')) {
            $donation->addMediaFromRequest('image')->toMediaCollection('donations');
        }
        return redirect('home')
            ->withSuccess(__('Your donation has been created successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\donation $donation
     * @return \Illuminate\Http\Response
     */
    public function show(donation $donation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\donation $donation
     * @return \Illuminate\Http\Response
     */
    public function edit(Donation $donation)
    {
        $projects = Category::where('type',2)->get();
        $accounts = Account::all();
        return view('donations.edit', compact('donation','projects','accounts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\donation $donation
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Donation $donation)
    {
        $data = request()->validate([
            'payment_type' => 'required',
            'date' => 'required',
            'account_id' => 'required',
            'amount' => 'required',
            'from_account' => 'nullable',
            'name' => 'required',
            'phone' => 'required',
            'category_id' => 'required',
        ]);

        $donation->update($data);

        if ($request->hasFile('image')) {
            $donation->media()->delete($this);
            $donation->addMediaFromRequest('image')->toMediaCollection('donations');
        }

        return redirect('donations/manage');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\donation $donation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Donation $donation)
    {
        $donation->delete();
        return redirect('donations/manage');

    }
}
