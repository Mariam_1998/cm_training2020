<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Facades\SEOMeta;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('auth')->only(['manage','create','store','edit','update','delete']);
        $this->middleware('admin')->only(['manage','create','store','edit','update','delete']);
    }

    public function manage()
    {
        return view('categories.manage', ['categories' => Category::all()]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('categories.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
        $data = $this->validate(
            $request, [
            'name' => 'required|unique:categories',
            'description' => 'nullable',
            'type' => 'nullable'
        ]);

        Category::create($data);
        return redirect('categories/manage')
            ->with('success', 'Your Category has been created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Category $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        SEOMeta::setTitle($category->name);
        SEOMeta::setDescription($category->description);
        SEOMeta::addKeyword(['Alber','جمعية البر بالاحساء','جمعية البر','ahsaber','مساعدات','مساعدة','محتاجين'
            ,'مشاريع','خيرية','دعم','تبرع','جمعية']);

        if($category->type === "3")
        {
            $projects = Category::where('type',2)->get();
            return view("categories.showCategories", compact('category', 'projects'));
        }else{

            $posts = $category->posts()//Post::where('category_id', $category->id)
                ->whereIsPublished(true)
                ->where('slug','<>','conditions')
                ->latest()
                ->simplePaginate(10);
            return view("categories.show", compact('category', 'posts'));
    }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Category $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Category $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $category->update($request->all());
        return redirect('categories/manage');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Category $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->posts()->delete();
        $category->delete();

        return redirect('categories/manage');
    }
}
