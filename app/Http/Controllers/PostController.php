<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Category;
use function React\Promise\all;
use Artesaos\SEOTools\Facades\SEOMeta;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {
        $this->middleware('auth')->only(['manage','create','store','edit','update','delete']);
        $this->middleware('admin')->only(['manage','create','store','edit','update','delete']);
    }

    public function index(Request $request)
    {
        SEOMeta::setTitle(__('Posts'));
        SEOMeta::addKeyword(['Alber','المنشورات','جمعية البر','ahsaber','مساعدات','مساعدة','محتاجين'
            ,'مشاريع','خيرية','دعم','تبرع','جمعية','طريقة التبرع']);

        $categories = Category::all()->except(4);
        $posts = Post::whereIsPublished(true)
            ->where('category_id','<>','3')
            ->latest()
            ->simplePaginate(10);
        return view('posts.index', compact('posts', 'categories'));
    }


    public function manage(Post $post)
    {
        return view('posts.manage', ['posts' => Post::all()]);
    }

    public function reorder()
    {
        $posts = Post::where('category_id',3)->get();
        return view('posts.reorder', compact('posts'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('posts.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->validate([
            'category_id' => 'required',
            'title' => 'required',
            'body' => 'required',
            'short_description'=>'nullable',
            'is_published' => 'nullable',
            'is_featured' => 'nullable',
        ]);
        $post = Post::create($data);

        if ($request->hasFile('image')) {
            $post->addMediaFromRequest('image')->toMediaCollection('posts');
        }

        return redirect('posts/manage')
            ->with('success', 'Your Post has been created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Post $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        SEOMeta::setTitle($post->title);
        SEOMeta::addKeyword(['Alber','منشور','المنشورات','جمعية البر','ahsaber','مساعدات','مساعدة','محتاجين'
            ,'خيرية','دعم','تبرع','جمعية']);

        $posts = Post::with('category')->get();

        return view('posts.show', compact('post','posts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categories = Category::all();
        return view('posts.edit', compact('post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Post $post
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Post $post)
    {
        $data = request()->validate([
            'category_id' => 'required',
            'title' => 'required',
            'body' => 'required',
            'short_description'=>'nullable',
            'is_published' => 'nullable',
            'is_featured' => 'nullable',
        ]);

        $post->update($data);
        if ($request->hasFile('image')) {
            $post->media()->delete($this);
            $post->addMediaFromRequest('image')->toMediaCollection('posts');
        }

        return redirect('posts/manage');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();
        return redirect('posts/manage');

    }
}
