<?php

namespace App\Http\Controllers;

use App\Models\Link;
use Illuminate\Http\Request;

class LinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('auth')->only(['manage','create','store','edit','update','delete']);
        $this->middleware('admin')->only(['manage','create','store','edit','update','delete']);
    }

    public function index()
    {
        $links = Link::whereNull('link_id')
            ->with('childLinks')
            ->orderby('title', 'asc')
            ->get();
        return view('links/manage', compact('links'));
    }

    public function manage()
    {
        return view('links/manage', ['links' => Link::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('links/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->validate([
            'title' => 'required',
            'URL' => 'required',
        ]);
         Link::create($data);
        return redirect('links/manage')
            ->with('success', 'Your Link has been created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Link $link
     * @return \Illuminate\Http\Response
     */
    public function show(Link $link)
    {
        $links = Link::all();
        return redirect('links')->with($links);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Link $link
     * @return \Illuminate\Http\Response
     */
    public function edit(Link $link)
    {
        return view('links/edit', compact('link'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Link $link
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Link $link)
    {
        $link->update($request->all());
        return redirect('links/manage');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Link $link
     * @return \Illuminate\Http\Response
     */
    public function destroy(Link $link)
    {
        $link->delete();
        return redirect('links/manage');
    }
}
