<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\Post;
use App\Notifications\RecievedMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Artesaos\SEOTools\Facades\SEOMeta;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        SEOMeta::setTitle(__('Home'));
        SEOMeta::addKeyword(['Alber','المنشورات','جمعية البر','ahsaber','مساعدات','مساعدة','محتاجين'
            ,'عطاء','مشاريع','خيرية','دعم','تبرع','جمعية','طريقة التبرع']);

        $featuredPosts = Post::whereIsPublished(true)
            ->whereIsFeatured(true)
            ->latest()
            ->get();

        $albums = Album::latest()
            ->get();

        return view('home', compact('featuredPosts','albums'));

    }

    public function contact()
    {
        return view('/contact');
    }

    public function send(Request $request)
    {
         $this->validate($request,
            [
                'email' => 'string|required',
                'phone' => 'string|required',
                'message' => 'string|required',

                'name' => 'string|required',
                'type' => 'string|required',
                'subject' => 'string|required',
            ]);

         if($request->hasFile('file'))
         {
             $url =$request->file;
         }

//dd($url);
//        foreach($request->file() as $file) {
//            $email->attach($file->getRealPath(), [
//                'as' => $file->getClientOriginalName(),
//                'mime' => $file->getMimeType(),
//            ]);
//        }

        Notification::route('mail', 'alber.cm.codes@gmail.com')
            ->notify(new RecievedMessage($request));
//                $url->getRealPath(), [
//                    'as' => $url->getClientOriginalName(),
//                    'mime' => $url->getMimeType(),
//                ])
//            ));
        //dd('hi');

        return back()->withSuccess(__('Your Message Has Been Delivered Successfully'));

    }

    public function manage()
    {
        $firstName = auth()->user()->first_name;
        $fatherName = auth()->user()->father_name;
        return view('/dashboard', compact('firstName', 'fatherName'));
    }
}
