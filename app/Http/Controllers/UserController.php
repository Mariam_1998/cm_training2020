<?php

namespace App\Http\Controllers;


use App\Actions\Fortify\PasswordValidationRules;
use App\Models\Order;
use App\Models\User;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Permission;

use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    use PasswordValidationRules;

//use PasswordValidationRules;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');//->only(['','','manage','edit','update','delete']);
    }


    public function index()
    {
        //
    }

    public function manage()
    {
        return view('users.manage', ['users' => User::all()]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Validator::make($request->all(), [
            'first_name' => ['required', 'string', 'max:255'],
            'father_name' => ['required', 'string', 'max:255'],
            'grandfather_name' => ['required', 'string', 'max:255'],
            'family_name' => ['required', 'string', 'max:255'],

            'id_number' => ['required', 'numeric', 'digits:10'],
            'card_issuer' => ['required', 'string', 'max:255'],
            'date' => ['required','date'],
            'reasons' => ['required', 'string'],
            'phone' =>['required', 'digits:10'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => $this->passwordRules(),
        ])->validate();

        request()->merge(['password' => Hash::make($request['password'])]);

        User::create(
            request()
                ->only( 'first_name', 'father_name', 'grandfather_name', 'family_name','password','email', 'password',
                    'id_number', 'card_issuer', 'date', 'reasons','phone'));
        return redirect('/users/manage');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $orders = $user->orders()->get();
        return view('users.show',compact('user','orders'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {

        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(User $user, Request $request)
    {

        Validator::make($request->all(),
            [
                'first_name' => ['required', 'string', 'max:255'],
                'father_name' => ['required', 'string', 'max:255'],
                'grandfather_name' => ['required', 'string', 'max:255'],
                'family_name' => ['required', 'string', 'max:255'],
                'phone' =>['required', 'digits:10'],
                'email' => ['required', 'email', 'max:255', Rule::unique('users')->ignore($user->id)],
                'id_number' => ['required', 'numeric', 'digits:10'],
                'card_issuer' => ['required', 'string', 'max:255'],
                'date' => ['required','date'],
                'reasons' => ['required', 'string'],
            ])->validateWithBag('updateProfileInformation');


        if (isset($request['photo'])) {
            $request->updateProfilePhoto($request['photo']);
        }

        if ($request['email'] !== $user->email &&
            $user instanceof MustVerifyEmail) {
            $this->updateVerifiedUser($user, $request);
        } else {
            $user->forceFill([
                'first_name' => $request['first_name'],
                'father_name' => $request['father_name'],
                'grandfather_name' => $request['grandfather_name'],
                'family_name' => $request['family_name'],
                'id_number' => $request['id_number'],
                'card_issuer' => $request['card_issuer'],
                'date' => $request['date'],
                'reasons' => $request['reasons'],
                'phone' => $request['phone'],
                'email' => $request['email'],

            ])->save();
        }

        return redirect('users/manage');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect('users/manage');
    }

}
