<?php

namespace App\Http\Middleware;

use Closure;
use http\Message;
use Illuminate\Http\Request;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(auth()->check())
        {
            if(!auth()->user()->is_admin)
            {
                auth()->logout();
                return redirect()->route('login')->with('message', trans('Your Account Needs Admin Role'));
            }
        }
        return $next($request);
    }
}
