<?php

namespace App\Http\Livewire;

use App\Models\Category;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class CategoriesDatatable extends LivewireDatatable
{
    public $model = Category::class;


    public function builder()
    {
        return Category::query();
    }

    public function columns()
    {
        return [
            NumberColumn::name('id')->label(__('#')),
            Column::name('name')
                ->label(__('Name'))->searchable(),

            Column::callback(['slug'], function ($slug) {
                return view('components.table-actions', ['url' => url("categories/$slug"), 'model' => 'Categories']);
            })
        ];
    }
}