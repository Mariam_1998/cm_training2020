<?php

namespace App\Http\Livewire;

use App\Models\Album;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class AlbumsDatatable extends LivewireDatatable
{
    public $model = Album::class;

    public function builder()
    {
        return Album::query();
    }

    public function columns()
    {
        return [
            NumberColumn::name('id')->label(__('#')),
            Column::name('title')
                ->label(__('Title'))->searchable(),

            Column::callback(['slug'], function ($slug) {
                return view('components.table-actions', ['url' => url("albums/$slug"), 'model' => 'Albums']);
            })
        ];
    }
}