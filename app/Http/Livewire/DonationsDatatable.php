<?php

namespace App\Http\Livewire;

use App\Models\Donation;
use App\Models\Account;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class DonationsDatatable extends LivewireDatatable
{
    public $model = Donation::class;

    public function builder()
    {
        return Donation::query()->with('category && account');
    }

    public function columns()
    {
        return [
            NumberColumn::name('id')->label(__('#')),

            Column::callback(['payment_type'], function ($type) {
                return __('payment_type_'.$type);
            })->searchable()->label(__('type').' '.(__('Payment'))),
            Column::name('category.name')
                ->label(__('name').' '.__('Project'))->searchable(),
            Column::name('account.name')->label(__('To Bank'))->searchable(),

            Column::name('date')->label(__('Date'))->searchable(),
            Column::name('amount')->label(__('Amount'))->searchable(),
            Column::name('name')->label(__('name').' '.__('Volunteer'))->searchable(),
            Column::name('phone')->label(__('Phone'))->searchable(),

            Column::callback(['id'], function ($id) {
                return view('components.table-actions', ['url' => url("donations/$id"), 'model' => 'Donations']);
            })
        ];
    }
}