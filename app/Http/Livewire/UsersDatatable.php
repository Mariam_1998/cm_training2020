<?php

namespace App\Http\Livewire;

use App\Models\User;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\BooleanColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class UsersDatatable extends LivewireDatatable
{
    public $model = User::class;

    public function builder()
    {
        return User::query();
    }

    public function columns()
    {
        return [
            NumberColumn::name('id')->label(__('#')),
            Column::name('first_name')->label( __('Name').' '.__('First'))->searchable(),
            Column::name('family_name')->label( __('name').' '.__('Family'))->searchable(),
            Column::name('id_number')->label( __('Number').' '. __('ID'))->searchable(),
            Column::name('reasons')->label(  __('Reasons For Registration'))->searchable(),
            Column::name('email')
                ->label(__('Email'))->searchable(),
            Column::name('phone')
                ->label(__('Phone'))->searchable(),

            Column::callback(['id'], function ($id) {
                return view('components.table-actions', ['url' => url("users/$id"), 'model' => 'Users']);
            })
        ];
    }
}
