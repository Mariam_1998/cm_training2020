<?php

namespace App\Http\Livewire;

use App\Models\Link;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class LinksDatatable extends LivewireDatatable
{
    public $model = Link::class;

    public function builder()
    {
        return Link::query();
    }

    public function columns()
    {
        return [
            NumberColumn::name('id')->label(__('#')),
            Column::name('title')
                ->label(__('Title'))->searchable(),
            Column::name('URL')->searchable(),
            Column::callback(['id'], function ($id) {
                return view('components.table-actions', ['url' => url("links/$id"), 'model' => 'Links']);
            })
        ];
    }
}
