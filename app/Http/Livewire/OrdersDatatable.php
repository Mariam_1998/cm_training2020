<?php

namespace App\Http\Livewire;

use App\Models\Order;
use App\Models\User;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class OrdersDatatable extends LivewireDatatable
{
    public $model = Order::class;

    public function builder()
    {
        return Order::query()->with('user');
    }

    public function columns()
    {
        return [
            NumberColumn::name('id')->label(__('#')),
            Column::callback(['user_id'], function ($user) {
                return optional(User::find($user))->full_name;
            })->label(__('username'))->searchable(),

            Column::name('user.phone')
                ->label(__('Phone'))->searchable(),

            Column::name('title')->searchable()->label(__('Title')),
            Column::name('problem')->searchable()->label(__('Problem')),
            Column::name('device')->searchable()->label(__('Device')),

            Column::callback(['type'], function ($type) {
                return __('order_'.$type);
            })->searchable()->label(__('Type')),

            Column::callback(['state'], function ($state) {
                return __('state_'.$state);
            })->searchable()->label(__('state').' '.__('Order')),


            Column::callback(['slug'], function ($slug) {
                return view('components.table-actions', ['url' => url("orders/$slug"), 'model' => 'Orders']);
            })
        ];
    }
}