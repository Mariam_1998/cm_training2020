<?php

namespace App\Http\Livewire;

use App\Models\Post;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\BooleanColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class PostsDatatable extends LivewireDatatable
{
    public $model = Post::class;


    public function builder()
    {
        return Post::query()->with('category');
    }

    public function columns()
    {
        return [
            NumberColumn::name('id')->label(__('#')),
            Column::name('title')->searchable()->label(__('Title')),

            Column::name('short_description')
                ->label(__('description').' '.__('Short'))->searchable(),
            Column::name('category.name')
                ->label(__('Category'))->searchable(),
            BooleanColumn::name('is_published')
                ->label(__('Is Published')),
            BooleanColumn::name('is_featured')
                ->label(__('Is Featured')),

            Column::callback(['slug'], function ($slug) {
                return view('components.table-actions', ['url' => url("posts/$slug"), 'model' => 'Posts']);
            })
        ];
    }
}
