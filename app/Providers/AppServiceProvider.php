<?php

namespace App\Providers;

namespace App\Providers;
use App\Models\Category;
use App\Models\Link;
use App\Models\Post;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        view()->composer('*', function ($view)
        {
            $ads = Post::where('category_id',3)
                ->whereIsPublished(true)
                ->latest()
                ->get();
            $links = Link::all();

            $categories = Category::where('type'," ")
                ->orwhere('type','3')
                ->get();
            $view->with('ads', $ads);
            $view->with('links', $links);
            $view->with('categories', $categories);
        });
    }
}
