<?php

namespace App\Actions\Fortify;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        Validator::make($input, [
            'first_name' => ['required', 'string', 'max:255'],
            'father_name' => ['required', 'string', 'max:255'],
            'grandfather_name' => ['required', 'string', 'max:255'],
            'family_name' => ['required', 'string', 'max:255'],

            'id_number' => ['required', 'numeric', 'digits:10'],
            'card_issuer' => ['required', 'string', 'max:255'],
            'date' => ['required','date'],
            'reasons' => ['required', 'string'],
            'phone' =>['required', 'digits:10'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => $this->passwordRules(),
        ])->validate();

        request()->merge(['password' => Hash::make($input['password'])]);

        return User::create(
            request()
                ->only( 'first_name', 'father_name', 'grandfather_name', 'family_name','password','email', 'password',
                    'id_number', 'card_issuer', 'date', 'reasons','phone'));
    }

    /**
     * Create a personal team for the user.
     *
     * @param  \App\Models\User  $user
     * @return void
     */

}
