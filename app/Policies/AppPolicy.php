<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AppPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the users can view any models.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the users can view the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\User $model
     * @return mixed
     */
    public function view(User $user, $model)
    {
        return true;
    }

    /**
     * Determine whether the users can create models.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->ableTo($user, 'Create');
    }

    /**
     * Determine whether the users can update the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\User $model
     * @return mixed
     */
    public function update(User $user, $model)
    {
        return $this->ableTo($user, 'Update');
    }

    /**
     * Determine whether the users can delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\User $model
     * @return mixed
     */
    public function delete(User $user, $model)
    {
        return $this->ableTo($user, 'Delete');
    }

    /**
     * Determine whether the users can restore the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\User $model
     * @return mixed
     */
    public function restore(User $user, $model)
    {
        return false;
    }

    /**
     * Determine whether the users can permanently delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\User $model
     * @return mixed
     */
    public function forceDelete(User $user, $model)
    {
        return false;
    }

    public function manage(User $user, $model)
    {
        if ($this->ableTo($user, 'Create', $model) or $this->ableTo($user, 'Update', $model) or $this->ableTo($user, 'Delete', $model) or $this->ableTo($user, 'History', $model))
            return true;
    }

    public function ableTo(User $user, $ability)
    {
        $modelName = request()->model;
        $hasPermission = $user->hasPermissionTo("$ability $modelName");

        if ($hasPermission) return true;

        return false;
    }
}
