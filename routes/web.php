<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'manage'])->name('dashboard');

Route::get('/categories/manage', [App\Http\Controllers\CategoryController::class, 'manage']);
Route::resource('categories', App\Http\Controllers\CategoryController::class);

Route::get('/posts/manage', [App\Http\Controllers\PostController::class, 'manage']);
Route::get('/posts/reorder', [App\Http\Controllers\PostController::class, 'reorder']);
Route::resource('posts', App\Http\Controllers\PostController::class);

Route::get('/donations/manage', [App\Http\Controllers\DonationController::class, 'manage']);
Route::resource('donations', App\Http\Controllers\DonationController::class);


Route::get('/orders/manage', [App\Http\Controllers\OrderController::class, 'manage']);
Route::resource('orders', App\Http\Controllers\OrderController::class);


Route::get('users/manage', [App\Http\Controllers\UserController::class, 'manage']);
Route::resource('users', App\Http\Controllers\UserController::class);

Route::get('links/manage', [App\Http\Controllers\LinkController::class, 'manage']);
Route::resource('links', App\Http\Controllers\LinkController::class);

Route::get('albums/manage', [App\Http\Controllers\AlbumController::class, 'manage']);
Route::resource('albums', App\Http\Controllers\AlbumController::class);

Route::get('/contact', [App\Http\Controllers\HomeController::class, 'contact']);
Route::post('/contact',[App\Http\Controllers\HomeController::class, 'send']);

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
