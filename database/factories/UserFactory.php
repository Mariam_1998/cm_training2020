<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $simpleEmail = $this->faker->randomLetter.'@'.$this->faker->randomLetter.'.com';
        return [
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'email' => $simpleEmail,
            'email_verified_at' => now(),
            'password' => Hash::make('12345678'),
            'remember_token' => Str::random(10),
            'phone'=>$this->faker->phoneNumber,
            'linkedin'=>$this->faker->url,
            'facebook'=>$this->faker->url,
            'is_public'=> $this->faker->boolean(),
            'is_active'=> $this->faker->boolean(),
            'university_id'=> $this->faker->numberBetween(1,10),
            'major_id'=> $this->faker->numberBetween(1,10),
        ];
    }
}
