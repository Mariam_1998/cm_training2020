<?php

namespace Database\Factories;

use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->title,
            'body'=> $this->faker->realText(),
            'short_description'=> $this->faker->realText(),
            'is_published'=>$this->faker->boolean(),
            'is_featured'=>$this->faker->boolean(),
            'category_id'=>$this->faker->numberBetween(1,5)
        ];
    }
}
