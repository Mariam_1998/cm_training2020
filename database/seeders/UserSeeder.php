<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'first_name' => 'S',
                'father_name' => 'S',
                'grandfather_name' => 'S',
                'family_name' => 'S',
                'is_admin'=>'1',
                'id_number' => '1234567890',
                'card_issuer' => 'الاحساء',
                'date' => '1/1/2021',
                'reasons' => 'Admin',
                'email' => 's@s.com',
                'email_verified_at' => now(),
                'password' => Hash::make('12345678'),
                'remember_token' => Str::random(10),
                'phone' => '0535010102',
                'slug'=>'s'
            ],
            [
                'first_name' => 'A',
                'father_name' => 'A',
                'grandfather_name' => 'A',
                'family_name' => 'A',
                'is_admin'=>'0',
                'id_number' => '1234567890',
                'card_issuer' => 'الاحساء',
                'date' => '2/2/2021',
                'reasons' => 'Admin',
                'email' => 'a@a.com',
                'email_verified_at' => now(),
                'password' => Hash::make('12345678'),
                'remember_token' => Str::random(10),
                'phone' => '0541192616',
                'slug'=>'a'
            ],
        ];
        User::insert($users);

    }
}
