<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use App\Models\Post;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategorySeeder::class);
        $this->call(PostSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(LinkSeeder::class);
        $this->call(AccountSeeder::class);
        // \App\Models\User::factory(10)->create();
    }
}
