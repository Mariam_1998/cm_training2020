<?php

namespace Database\Seeders;

use App\Models\Link;
use Illuminate\Database\Seeder;

class LinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $links = [
            [
                'title' => __('Home'),
                'URL' => '/home'
            ],
            [
                'title' => __('About us'),
                'URL' => '/posts/aana'
            ],
            [
                'title' => __('Our services'),
                'URL' => '/posts/khdmatna'
            ],
            [
                'title' => __('Team'),
                'URL' => '/posts/fryk-alaaml'
            ],
            [
                'title' => __('Supporters'),
                'URL' => '/posts/aldaaamyn'
            ],
            [
                'title' => __('Contact Us'),
                'URL' => '/contact'
            ],

        ];
        Link::insert($links);
    }
}
