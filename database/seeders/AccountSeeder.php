<?php

namespace Database\Seeders;

use App\Models\Account;
use Illuminate\Database\Seeder;

class AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $accounts = [
            ['name' => __('Samba')],
            ['name' => __('Sabb')],
            ['name' => __('Rajhi')],
            ['name' => __('Investment')],
            ['name' => __('Anb')],
            ['name' => __('Albilad')],
        ];
        Account::insert($accounts);
    }
}
