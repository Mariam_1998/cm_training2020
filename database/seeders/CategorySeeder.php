<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category =[
            ['name'=>__('Posts'),'description'=>'','type'=>'', 'slug'=>__('Posts')],
            ['name'=>__('News'),'description'=>'','type'=>'','slug'=>__('News')],
            ['name'=>__('Ads'),'description'=>'','type'=>'', 'slug'=>__('Ads')],
            ['name'=>__('Projects'),'description'=>'','type'=>'3', 'slug'=>__('Projects')],

            ['name'=>'رعاية الأيتام',
                'description'=>'من هو اليتيم؟
ذكره الله في كتابه فقال “وأما اليتيم فلا تقهر“
وذكره النبي الكريم فقال “أنا وكافل اليتيم في الجنة كهاتين وأشار بالسبابة والوسطى وفرق بينهما قليلاً“

لم يحصل اليتيم على هذه المنزلة إلا لصعوبة المعاناة التي يعيشها،

في ظل حربٍ قاسيةٍ أكثر ما يجتاج فيها الطفل لوالديه ليشعرونه بالأمان،

ففي حياة اليتم قصصٌ مؤثرة ومواقف لا تنسى، لكنه أمام كل هذا يبقى يبتسم ويحاول أن يقف إن تعثر.

فاليتم طفلٌ حرم من أن ينادي كلمة أبي

من أن يكون له شخص يساعده على النهوض كلما وقع.

من أن يكون له أم تداويه إن مرض

ومن لم يعش اليتم لن يعرف معنى الإحساس المؤلم الذي يسكن روحه،

لذا حاول أن تغمض إحدى عينيك وتخيل أنك فقدت الأب، ومن ثم أغمض العين الآخرى وتخيل أنك في ليلة وضحاها فقدت الأم،

ترى كم هو مقدار الألم الذي ستعيشه، ونحن مازلنا بمجرد خيال، فكيف حال من كان هذا واقعٌ فرض عليه.

اليتيم في الحرب، طفلٌ فقد ماضيه وهو مهيأ لفقد المستقبل،

ما لم يجد أيدٍ تعطف عليه وتعينه على عبور هذه المحطة المؤلمة من حياته.',
                'type'=>'2',
                'slug'=>'رعاية الأيتام'],

            ['name'=>'دعم المقبلين على الزواج',
                'description'=>'مساعدة الشباب المقبلين على الزواج في تحمل تكاليف وأعباء الزواج المادية وتوعيتهم من خلال ندوات ومحاضرات تبين لكلا الجنسين كيفية إختيار الزوج والزوجة في ظل الظروف الاقتصادية والاجتماعية ',
                'type'=>'2',
                'slug'=>'دعم المقبلين على الزواج'],

            ['name'=>'الدعم المالي للأسر المحتاجة',
                'description'=>'المشروع عبارة عن دعم وتثبيت العائلات المقدسية الفقيرة في مدينة القدس والبلدة القديمة بمجموعة من المشاريع التنموية التي تنهض بالأسر الفقيرة وتعينها على الثبات أمام مخططات الاحتلال وهمجيته',
                'type'=>'2',
                'slug'=>'الدعم المالي للأسر المحتاجة'],

        ];

        \DB::table('categories')->insert($category);
    }
}
